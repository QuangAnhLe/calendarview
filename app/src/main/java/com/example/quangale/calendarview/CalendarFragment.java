package com.example.quangale.calendarview;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.Date;
import java.util.UUID;

/**
 * A simple {@link Fragment} subclass.
 */
public class CalendarFragment extends Fragment {
    public static final String EXTRA_CALENDAR_ID = "com.example.quangale.calendarview.calendar_id";
    private static final String DIALOG_DATE = "date";
    //private static final int REQUEST_DATE = 0;
    private static final String DIALOG_TIME_BEGIN = "time begin";
    private static final String DIALOG_TIME_END = "time end";
    private CalendarObject mCalendarObject;
    private EditText mTitleFiel;
    private EditText mPlaceFiel;
    private Button mDateButon;
    private Button mTimeBegin;
    private Button mTimeEnd;
    private CheckBox mSolvedCheckBox;

    public static CalendarFragment newInstance(UUID calendarId){
        Bundle args = new Bundle();
        args.putSerializable(EXTRA_CALENDAR_ID, calendarId);

        CalendarFragment fragment = new CalendarFragment();
        return fragment;
    }
    public void returnResult(){
        getActivity().setResult(Activity.RESULT_OK,null);
    }
    public void updateDate(){
        mDateButon.setText(mCalendarObject.getDate().toString());
    }


    public CalendarFragment() {
        // Required empty public constructor
    }
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        UUID calendarId = (UUID) getActivity().getIntent()
                .getSerializableExtra(EXTRA_CALENDAR_ID);
        /*UUID calendarId = (UUID)getArguments().getSerializable(EXTRA_CALENDAR_ID);
        mCalendarObject = CalendarLab.get(getActivity()).getCalendar(calendarId);*/
        mCalendarObject = new CalendarObject();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent,
                             Bundle savedInstanceState){
        View v = inflater.inflate(R.layout.fragment_calendar,parent,false);

        mTitleFiel = (EditText) v.findViewById(R.id.calendar_titel);
        mTitleFiel.setText(mCalendarObject.getTitle());
        mTitleFiel.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                mCalendarObject.setTitle(s.toString());
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // This space intentionally left blank
            }

            @Override
            public void afterTextChanged(Editable s) {
                // This one too
            }
        });


        mPlaceFiel = (EditText) v.findViewById(R.id.calendar_place);
        mTitleFiel.setText(mCalendarObject.getPlace());
        mPlaceFiel.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                mCalendarObject.setPlace(s.toString());
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // This space intentionally left blank
            }

            @Override
            public void afterTextChanged(Editable s) {
                // This one too
            }
        });

        // Showing a DialogFragment for date
        mDateButon = (Button)v.findViewById(R.id.calendar_date);
        mDateButon.setText(mCalendarObject.getDate().toString());
        //mDateButon.setEnabled(false);
        //updateDate();
        mDateButon.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                FragmentManager fm = getActivity()
                        .getSupportFragmentManager();
                DatePickerFragment dialog = new DatePickerFragment();
                //DatePickerFragment dialog = DatePickerFragment
                //        .newInstance(mCalendarObject.getDate());   // call to newInstance on the DatePickerFragment
                //dialog.setTargetFragment(CalendarFragment.this,REQUEST_DATE);
                dialog.show(fm, DIALOG_DATE);
            }
        });

        // Showing a DialogFragment for time begin
        mTimeBegin = (Button)v.findViewById(R.id.calendar_time_begin);
        mTimeBegin.setText(mCalendarObject.getTimeBegin().toString());
        //mTimeBegin.setEnabled(false);
        mTimeBegin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity()
                        .getSupportFragmentManager();
                TimeBeginPickerFragment dialog = new TimeBeginPickerFragment();
                dialog.show(fm,DIALOG_TIME_BEGIN);
            }
        });
        // Showing a DialogFragment for time end
        mTimeEnd = (Button)v.findViewById(R.id.calendar_time_end);
        mTimeEnd.setText(mCalendarObject.getTimeEnd().toString());
        //mTimeEnd.setEnabled(false);
        mTimeEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getActivity()
                        .getSupportFragmentManager();
                TimeEndPickerFragment dialog = new TimeEndPickerFragment();
                dialog.show(fm, DIALOG_TIME_END);

            }
        });

        mSolvedCheckBox = (CheckBox)v.findViewById(R.id.calendar_saved);
        mSolvedCheckBox.setChecked(mCalendarObject.isSaved());
        mSolvedCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mCalendarObject.setSaved(isChecked);
            }
        });
        return v;


    }
    /*@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode != Activity.RESULT_OK) return;
        if(requestCode == REQUEST_DATE){
            Date date = (Date)data.getSerializableExtra(DatePickerFragment.EXTRA_DATE);
            mCalendarObject.setDate(date);
            updateDate();
        }
    }*/


}
