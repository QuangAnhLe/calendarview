package com.example.quangale.calendarview;
import java.util.Date;
import java.util.Timer;
import java.util.UUID;

public class CalendarObject {
    private UUID mId;
    private String mTitle;
    private String mPlace;
    private Date mDate;
    private Timer mTimeBegin;
    private Timer mTimeEnd;
    private boolean mSaved;

    public CalendarObject(){
        //Generate unique identifier
        mId = UUID.randomUUID();
        mDate = new Date();
        mTimeBegin = new Timer();
        mTimeEnd   = new Timer();
    }
    @Override
    public String toString(){
        return "Title: "+ mTitle + "  Place: " + mPlace;
    }

    public UUID getId() {
        return mId;
    }
    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }
    public String getPlace() {
        return mPlace;
    }

    public void setPlace(String mPlace) {
        this.mPlace = mPlace;
    }
    public Date getDate() {
        return mDate;
    }

    public void setDate(Date date) {
        this.mDate = date;
    }
    public Timer getTimeBegin() {
        return mTimeBegin;
    }

    public void setTimeBegin(Timer timeBegin) {
        this.mTimeBegin = timeBegin;
    }
    public Timer getTimeEnd() {
        return mTimeEnd;
    }

    public void setTimeEnd(Timer timeEnd) {
        this.mTimeEnd = timeEnd;
    }
    public boolean isSaved() {
        return mSaved;
    }

    public void setSaved(boolean saved) {
        this.mSaved = saved;
    }

}
