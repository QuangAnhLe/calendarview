package com.example.quangale.calendarview;

import android.support.v4.app.Fragment;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

public class CalendarListActivity extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment(){
        return new CalendarListFragment();
        }
}
