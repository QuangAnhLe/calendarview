package com.example.quangale.calendarview;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

public class TimeBeginPickerFragment extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        return new AlertDialog.Builder(getActivity())
                .setTitle(R.string.timeBegin_picker_title)
                .setPositiveButton(android.R.string.ok,null)
                .create();
    }
}