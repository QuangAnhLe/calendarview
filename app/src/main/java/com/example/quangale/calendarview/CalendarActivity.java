package com.example.quangale.calendarview;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;

import java.util.UUID;

public class CalendarActivity extends SingleFragmentActivity {
    /** Called when the activity ist first create*/
    @Override
    protected Fragment createFragment(){
        //return new CalendarFragment();
        // call newInstance
        UUID calendarId = (UUID)getIntent()
                .getSerializableExtra(CalendarFragment.EXTRA_CALENDAR_ID);

        return CalendarFragment.newInstance(calendarId);
    }
    /*protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);

        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragmentContainer);

        if(fragment==null){
            fragment = new CalendarFragment();
            fm.beginTransaction().add(R.id.fragmentContainer,fragment)
                    .commit();
        }
    }
    */


}
