package com.example.quangale.calendarview;

import android.content.Context;

import java.util.ArrayList;
import java.util.UUID;

public class CalendarLab {

    private ArrayList<CalendarObject> mCalendars;

    private static CalendarLab sCalendarLab;
    private Context mAppContext;

    private CalendarLab(Context appContext){
        mAppContext = appContext;
        mCalendars = new ArrayList<CalendarObject>();
        for(int i=0; i<100; i++){
            CalendarObject c = new CalendarObject();
            c.setTitle("Calendar #"+i);
            mCalendars.add(c);
            c.setPlace("Place #"+ i);
            //mCalendars.add(c);
            c.setSaved(i%2==0); // Every other one
        }
    }

    public static CalendarLab get(Context c){
        if(sCalendarLab == null){
            sCalendarLab = new CalendarLab(c.getApplicationContext());
        }
        return sCalendarLab;
    }

    public ArrayList<CalendarObject> getCalendars(){
        return mCalendars;
    }

    public CalendarObject getCalendar(UUID id){
        for(CalendarObject c: mCalendars){
            if(c.getId().equals(id))
                return c;
        }
        return null;
    }

}

