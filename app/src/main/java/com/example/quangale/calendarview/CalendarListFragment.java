package com.example.quangale.calendarview;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class CalendarListFragment extends ListFragment {
    private static final int REQUEST_CALENDAR =1;
    private ArrayList<CalendarObject> mCalendars;
    private static final String TAG = "CalendarListFragment";

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        getActivity().setTitle(R.string.calendar_title);
        mCalendars = CalendarLab.get(getActivity()).getCalendars();
        //getActivity().setTitle(R.string.calendar_place);
        //mCalendars = CalendarLab.get(getActivity()).getCalendars();

        //ArrayAdapter<CalendarObject> adapter =
        //        new ArrayAdapter<CalendarObject>(getActivity(),
        //                android.R.layout.simple_list_item_1,
        //                mCalendars);
        CalendarAdapter adapter = new CalendarAdapter(mCalendars);
        setListAdapter(adapter);

    }
    @Override
    public void onListItemClick(ListView l, View v, int position, long id){
        //CalendarObject c = (CalendarObject) (getListAdapter().getItem(position));
        // Get the calendar from adapter
        CalendarObject c = ((CalendarAdapter)getListAdapter()).getItem(position);
        Log.d(TAG, c.getTitle()+ "was clicked");
        //Start CalendarActivity
        Intent i = new Intent(getActivity(),CalendarActivity.class);
        i.putExtra(CalendarFragment.EXTRA_CALENDAR_ID,c.getId());
        startActivity(i);
        //startActivityForResult(i,REQUEST_CALENDAR);
    }
    @Override
    public void onResume(){
        super.onResume();
        ((CalendarAdapter)getListAdapter()).notifyDataSetChanged();
    }

    private class CalendarAdapter extends ArrayAdapter<CalendarObject>{
        public CalendarAdapter(ArrayList<CalendarObject> calendars){
            super(getActivity(),0, calendars);
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent){
            // If we weren't given a view, inflate one
            if(convertView == null){
                convertView = getActivity().getLayoutInflater()
                        .inflate(R.layout.list_item_crime, null);
            }
            // Configure the view for this Crime
            CalendarObject c = getItem(position);

            TextView titleTextView =
                    (TextView) convertView.findViewById(R.id.calendar_list_item_titleTextView);
            titleTextView.setText(c.getTitle());
            TextView placeTextView =
                    (TextView) convertView.findViewById((R.id.calendar_list_item_placeTextView));
            placeTextView.setText(c.getPlace());
            TextView dateTextView  =
                    (TextView) convertView.findViewById(R.id.calendar_list_item_dateTextView);
            dateTextView.setText(c.getDate().toString());
            TextView timeBeginView =
                    (TextView) convertView.findViewById(R.id.calendar_list_item_timeBeginTextView);
            timeBeginView.setText(c.getTimeBegin().toString());
            TextView timeEndView =
                    (TextView) convertView.findViewById(R.id.calendar_list_item_timeEndTextView);
            timeEndView.setText(c.getTimeBegin().toString());
            CheckBox savedCheckBox =
                    (CheckBox) convertView.findViewById(R.id.calendar_list_item_savedCheckBox);
            savedCheckBox.setChecked(c.isSaved());

            return convertView;
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode==REQUEST_CALENDAR){
            //Handle result
        }
    }

}
