package com.example.quangale.calendarview;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import java.util.ArrayList;
import java.util.UUID;

public class CalendarPagerActivity extends FragmentActivity {
    private ViewPager mViewPager;
    private ArrayList<CalendarObject> mCalendars;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        mViewPager = new ViewPager(this);
        mViewPager.setId(R.id.viewPager);
        setContentView(mViewPager);

        mCalendars = CalendarLab.get(this).getCalendars();

        FragmentManager fm = getSupportFragmentManager();
        mViewPager.setAdapter(new FragmentStatePagerAdapter(fm) {
            @Override
            public Fragment getItem(int pos) {
                CalendarObject calendar = mCalendars.get(pos);
                return CalendarFragment.newInstance(calendar.getId());
            }

            @Override
            public int getCount() {
                return mCalendars.size();
            }
        });

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int pos, float posOffset, int posOffsetPixels) {

            }

            @Override
            public void onPageSelected(int pos) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {
                CalendarObject calendar = mCalendars.get(pos);
                if(calendar.getTitle() != null){
                    setTitle(calendar.getTitle());
                }

            }
        });


        UUID calendarId = (UUID)getIntent()
                .getSerializableExtra(CalendarFragment.EXTRA_CALENDAR_ID);
        for(int i=0; i<mCalendars.size();i++){
            if(mCalendars.get(i).getId().equals(calendarId)){
                mViewPager.setCurrentItem(i);
                break;
            }
        }
    }
}
